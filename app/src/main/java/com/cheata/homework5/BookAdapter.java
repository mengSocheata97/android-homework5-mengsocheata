package com.cheata.homework5;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import java.util.Objects;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.BookViewHolder> {
    private List<Book> list;

    BookAdapter(List<Book> bookList) {
        this.list = bookList;
    }
    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflater = LayoutInflater.from(parent.getContext()).inflate(R.layout.book,parent,false);
        return new BookViewHolder(inflater);
    }
    public void onBindViewHolder(@NonNull final BookViewHolder holder, int position) {
        final Book book = list.get(position);

        final byte[] bytes = book.getThumbnail();
        if(bytes == null) {
            holder.picCard.setImageResource(R.drawable.lisa);
        } else {
            holder.picCard.setImageBitmap(Helper.getImage(bytes));
        }


        holder.bookTitle.setText(book.getBookTitle());
        holder.bookPrice.setText(book.getBookPrice());
        holder.bookSize.setText(book.getBookSize());
            holder.threeDot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    PopupMenu popupMenu = new PopupMenu(v.getContext(), holder.threeDot);
                    popupMenu.inflate(R.menu.more_menu);
                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()){
                                case R.id.remove:
                                    Toast.makeText(v.getContext(), "Book has been removed", Toast.LENGTH_LONG).show();
                                    Client.getInstance(v.getContext())
                                            .getBookDatabase()
                                            .getBookDao()
                                            .deleteBook(book);
                                    ((MainActivity) Objects.requireNonNull(v.getContext())).refreshData();
                                    break;
                                case R.id.edit:
                                    Toast.makeText(v.getContext(), "Update", Toast.LENGTH_LONG).show();
                                    EditDialog editDialog = new EditDialog(book);
                                    editDialog.show(
                                            ((FragmentActivity)v.getContext()).getSupportFragmentManager().beginTransaction(),
                                            "EditDialog"
                                    );
                                    break;
                                case R.id.read:
                                    Toast.makeText(v.getContext(), "Read", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(v.getContext(),ReadActivity.class);
                                    intent.putExtra("bookId",book.getBookId());
                                    v.getContext().startActivity(intent);
                            }
                            return false;
                        }
                    });
                    popupMenu.show();
                }
            });
        }
    @Override
    public int getItemCount() {
        return list.size();
    }
    class BookViewHolder extends RecyclerView.ViewHolder {

        ImageView picCard;
        TextView bookTitle,bookPrice,bookSize;
        TextView threeDot;

        public BookViewHolder(@NonNull View itemView) {
            super(itemView);
            picCard = itemView.findViewById(R.id.pic_card);
            bookTitle = itemView.findViewById(R.id.book_title);
            bookPrice = itemView.findViewById(R.id.book_price);
            bookSize = itemView.findViewById(R.id.book_size);
            threeDot = itemView.findViewById(R.id.three_dot);
        }
    }

    public void updateList(List<Book> bookList){
        if(bookList!=null){
            list.clear();
            this.list = bookList;
            this.notifyDataSetChanged();
        }
    }
}



