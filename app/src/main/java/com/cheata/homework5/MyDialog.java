package com.cheata.homework5;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.thedeanda.lorem.LoremIpsum;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;

import static android.app.Activity.RESULT_OK;

public class MyDialog extends DialogFragment {
    private final int GALLERY_REQUErST_CODE = 1;
    private ImageView imageView;
    private EditText txtTitle,txtPrice,txtSize;
    private Button btnCancel,btnSave;
    private Spinner spinner;
    private  Uri selectedImage;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_dialog,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        spinner = view.findViewById(R.id.spinner_categories);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),R.array.book_type,android.R.layout.simple_list_item_1);
        spinner.setAdapter(adapter);

        imageView = view.findViewById(R.id.book_image);
        txtPrice = view.findViewById(R.id.edit_price);
        txtSize = view.findViewById(R.id.edit_size);
        txtTitle = view.findViewById(R.id.edit_title);
        btnCancel = view.findViewById(R.id.btn_cancel);
        btnSave = view.findViewById(R.id.btn_save);

        FloatingActionButton floatingActionButton = view.findViewById(R.id.fl_btn_pick_image);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                pickIntent.setType("image/*");
                startActivityForResult(pickIntent,1);
            }
        });
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK && null != data) {
            selectedImage = data.getData();
            imageView.setImageURI(selectedImage);
        }
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Objects.requireNonNull(getDialog()).dismiss();
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                byte[] inputStream;
                InputStream iStream;
                try {
                    if(selectedImage == null) {
                        inputStream = null;
                    }
                    else {
                        iStream = getActivity().getContentResolver().openInputStream(selectedImage);
                        inputStream = Helper.getBytes(iStream);
                    }

                    Book book = new Book(
                            txtTitle.getText().toString(),
                            txtPrice.getText().toString(),
                            txtSize.getText().toString(),
                            inputStream
                    );
                    Log.e("Insert_Book", book.toString());
                    Client.getInstance(getContext())
                            .getBookDatabase()
                            .getBookDao()
                            .addBook(book);
                    Objects.requireNonNull(getDialog()).dismiss();

                    ((MainActivity) Objects.requireNonNull(getActivity())).refreshData();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });

    }
}
