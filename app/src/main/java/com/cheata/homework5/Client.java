package com.cheata.homework5;

import android.content.Context;

import androidx.room.Room;

public class Client {

    private static Client myInstance;
    private Context mContext;
    private BookDatabase bookDatabase;

    public BookDatabase getBookDatabase(){
        return bookDatabase;
    }
    public static synchronized Client getInstance(Context context){
        if(myInstance==null) myInstance = new Client(context);
        return myInstance;
    }
    private Client(Context mContext){
        this.mContext = mContext;
        bookDatabase = Room.databaseBuilder(mContext,BookDatabase.class,"books")
                .allowMainThreadQueries()
                .build();
    }
}
