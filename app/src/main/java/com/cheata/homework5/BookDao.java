package com.cheata.homework5;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface BookDao {
    @Query("SELECT * FROM books")
      List<Book> getBookList();

    @Delete
    void deleteBook(Book book);

    @Query("SELECT * FROM books WHERE bookTitle LIKE :bookTitle")
    List<Book> searchBook(String bookTitle);

    @Insert
    void addBook(Book... book);

    @Update
    void editBook(Book book);

    @Query("SELECT * FROM books WHERE bookId =:bookId")
    Book findBookById(int bookId);



}
