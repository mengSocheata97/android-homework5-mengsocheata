package com.cheata.homework5;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.thedeanda.lorem.Lorem;
import com.thedeanda.lorem.LoremIpsum;

public class ReadActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read);

        int bookId =  getIntent().getIntExtra("bookId",1);



        Book book = Client.getInstance(getApplicationContext())
                .getBookDatabase()
                .getBookDao()
                .findBookById(bookId);
        TextView title= findViewById(R.id.read_title);
        title.setText(book.getBookTitle());
        title.setAllCaps(true);

        final byte[] bytes = book.getThumbnail();
        ImageView imageView = findViewById(R.id.read_image);
        if(bytes == null) {
            imageView.setImageResource(R.drawable.lisa);
        } else {
            imageView.setImageBitmap(Helper.getImage(bytes));
        }

        TextView desc = findViewById(R.id.read_des);
        Lorem ipsum = LoremIpsum.getInstance();
        desc.setText(ipsum.getHtmlParagraphs(1,5));
    }
}
