package com.cheata.homework5;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Arrays;

@Entity( tableName="books")
public class Book {
    @PrimaryKey(autoGenerate = true)
    private int bookId;
    @ColumnInfo(name = "bookTitle")
    private String bookTitle;
    @ColumnInfo(name = "bookPrice")
    private String bookPrice;
    @ColumnInfo(name = "bookSize")
    private String bookSize;

    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    private byte[] thumbnail;

    public Book() {}


    public Book(String bookTitle, String bookPrice, String bookSize, byte[] thumbnail) {
        this.bookTitle = bookTitle;
        this.bookPrice = bookPrice;
        this.bookSize = bookSize;
        this.thumbnail = thumbnail;
    }

    public int getBookId() {return  bookId;}

    public void setBookId(int bookId) { this.bookId = bookId;}

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public String getBookPrice() {
        return bookPrice;
    }

    public void setBookPrice(String bookPrice) {
        this.bookPrice = bookPrice;
    }

    public String getBookSize() {
        return bookSize;
    }

    public void setBookSize(String bookSize) {
        this.bookSize = bookSize;
    }

    public byte[] getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(byte[] thumbnail) {
        this.thumbnail = thumbnail;
    }

    @Override
    public String toString() {
        return "Book{" +
                "bookId=" + bookId +
                ", bookTitle='" + bookTitle + '\'' +
                ", bookPrice='" + bookPrice + '\'' +
                ", bookSize='" + bookSize + '\'' +
                ", thumbnail=" + Arrays.toString(thumbnail) +
                '}';
    }
}
