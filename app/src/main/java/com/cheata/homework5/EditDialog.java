package com.cheata.homework5;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Objects;

public class EditDialog extends DialogFragment {
    private ImageView imageView;
    private EditText txtTitle,txtPrice,txtSize;
    private Button btnCancel,btnSave;
    private Spinner spinner;
    private Book book;

    EditDialog(Book book){
        this.book = book;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.edit_dialog,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        spinner = view.findViewById(R.id.edit_spinner_categories);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),R.array.book_type,android.R.layout.simple_list_item_1);
        spinner.setAdapter(adapter);
//        spinner.setSelection(adapter.getPosition(book.get));

        imageView = view.findViewById(R.id.edit_image);
        txtPrice = view.findViewById(R.id.etxt_edit_price);
        txtPrice.setText(book.getBookPrice());
        txtSize = view.findViewById(R.id.etxt_edit_size);
        txtSize.setText(book.getBookSize());
        txtTitle = view.findViewById(R.id.etxt_edit_title);
        txtTitle.setText(book.getBookTitle());
        btnCancel = view.findViewById(R.id.edit_btn_cancel);
        btnSave = view.findViewById(R.id.edit_btn_save);

        FloatingActionButton floatingActionButton = view.findViewById(R.id.fl_btn_pick_image_edit);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                pickIntent.setType("image/*");
                startActivityForResult(pickIntent,1);
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                book.setBookTitle(txtTitle.getText().toString());
                book.setBookPrice(txtPrice.getText().toString());
                book.setBookSize(txtSize.getText().toString());
                Client.getInstance(getContext())
                        .getBookDatabase()
                        .getBookDao()
                        .editBook(book);

                ((MainActivity) Objects.requireNonNull(getActivity())).refreshData();

                Objects.requireNonNull(getDialog()).dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Objects.requireNonNull(getDialog()).dismiss();
            }
        });

    }
}
