package com.cheata.homework5;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class SearchFragment extends Fragment {

    private Button btnSearch,btnDonate;
    private EditText search;
    private SearchBook searchBook;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        btnSearch = view.findViewById(R.id.btn_search_book);
        btnDonate = view.findViewById(R.id.btn_donate_book);
        search= view.findViewById(R.id.search);
        return view;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchBook.bookSearch(search.getText().toString());
            }
        });

        btnDonate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog();
            }
        });
    }
    public void alertDialog()
    {
        MyDialog myDialog = new MyDialog();
        myDialog.show(getFragmentManager(),"example");
    }

    interface SearchBook{
        void bookSearch(String search);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        searchBook=(SearchBook)getActivity();
    }
}
