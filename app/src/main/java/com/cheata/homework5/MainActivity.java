package com.cheata.homework5;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.GridLayout;
import android.widget.RelativeLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements SearchFragment.SearchBook {
    private BookAdapter bookAdapter;
    private BottomNavigationView bottomNavigationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.fr_search,new SearchFragment())
                .commit();

        bookAdapter = new BookAdapter(
                Client.getInstance(MainActivity.this)
                .getBookDatabase()
                .getBookDao()
                .getBookList()
        );

        bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                if(menuItem.getItemId()== R.id.home)
                {
                    refreshData();
                }
                return true;
            }
        });

        RecyclerView recyclerView = findViewById(R.id.re_display_book);
        recyclerView.setLayoutManager(new GridLayoutManager(this,2));
        recyclerView.setAdapter(bookAdapter);
    }

    public void refreshData(){
        bookAdapter.updateList(Client.getInstance(MainActivity.this)
                .getBookDatabase()
                .getBookDao()
                .getBookList());
    }

    @Override
    public void bookSearch(String search) {
        Log.i("search", "bookSearch: "+search);
        bookAdapter.updateList(
                Client.getInstance(getApplicationContext())
                .getBookDatabase()
                .getBookDao()
                .searchBook(search)
        );
    }
}
