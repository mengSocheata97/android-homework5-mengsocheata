package com.cheata.homework5;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class Helper {

    public static byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int size = 1024;
        byte[] buf = new byte[size];

        int length = 0;
        while ((length = inputStream.read(buf)) != -1) {
            byteBuffer.write(buf, 0, length);
        }
        return byteBuffer.toByteArray();
    }
    public static Bitmap getImage(byte[] img) {
        return BitmapFactory.decodeByteArray(img, 0, img.length);
    }
}

